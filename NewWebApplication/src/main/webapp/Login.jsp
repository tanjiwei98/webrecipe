<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
<div>
	<a href="ListServlet.java"><h1>Jw Recipe Web app</h1></a>
	<fieldset>
			<legend><h1>Login</h1></legend>
			<%
				String status = (String)request.getAttribute("status");
				if(status != null)
				{
					out.println(status);
				}
			%>
			<form onsubmit="return validate()" action="LoginServlet" method="post">
				<table>
					<tr>
						<td>Email : </td>
						<td><input type="email" id="email" name="emailid"></td>
					</tr>
					<tr>
						<td>Password : </td>
						<td><input type="password" id="pass" name="password"></td>
					</tr>
					
					<tr>
						<td><input type="submit" value="login"></td>
					</tr>
					
				</table>
			</form>
			<br>
			<hr>
			New user? <a href="Registration.jsp">Click Here</a>
		</fieldset>
</div>
</body>
</html>