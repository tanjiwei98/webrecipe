<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src='http://js.nicedit.com/nicEdit-latest.js' type="text/javascript"></script>
<script>
bkLib.onDomLoaded(function() { nicEditors.allTextAreas()});
</script>
<script type="text/javascript">
	function validate()
	{
		 var name=document.getElementById("name").value;
		 var image=document.getElementById("fileUpload").value;
		 var ing=document.getElementById("ingredient").value;
		 var step=document.getElementById("step").value;
		 
		 if(name=="")
		 {
			 document.getElementById("nameerror").innerHTML="Please enter name of the dish.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("nameerror").innerHTML="";
		 }
		 if(ing=="")
		 {
			 document.getElementById("ingredienterror").innerHTML="Please enter the ingredient.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("ingredienterror").innerHTML="";
		 }
		 if(step=="")
		 {
			 document.getElementById("steperror").innerHTML="Please enter the step.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("steperror").innerHTML="";
		 }
		 return true;
	}
	
</script>
</head>
<body>
	<a href="ListServlet.java"><h1>Jw Recipe Web app</h1></a>
	<div align="center">
		<h3>Edit Recipe</h3>
			<h3>${recipe.name}</h3>
		<form onsubmit="return validate()" action="UpdateServlet" method="post">
			<INPUT type="hidden" name="id" value="${recipe.id}" id="id" />
			<img src="data:image/jpg;base64,${recipe.img64}" width="240" height="300"/><br>
			<h3>Ingredient : </h3>
			<span style="color:red" id="ingredienterror">*</span><br><br>
			<textarea rows="30" cols="50" name="ingredient"id="ingredient" >
			<c:out value="${recipe.ingredient}"></c:out></textarea><br>
			<h3>Step : </h3>
			<span style="color:red" id="steperror">*</span><br><br>
			<textarea rows="30" cols="50" name="step" id="step" >
			<c:out value="${recipe.step}"></c:out></textarea>
			<br>
			
			
			<input type="submit" value="Update"><br><br>
			
		</form>
		<a href="details?id=<c:out value='${recipe.id}'/>"><input type="submit" class="button" value="Cancel"></a><br><br>
	</div>

</body>
</html>