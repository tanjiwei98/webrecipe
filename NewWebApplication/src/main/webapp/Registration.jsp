<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration</title>

<script type="text/javascript">
	function validate()
	{
		 var email=document.getElementById("email").value;
		 var pass=document.getElementById("pass").value;
		 var cpass=document.getElementById("cpass").value;
		 
		 if(email=="")
		 {
			 document.getElementById("emailerror").innerHTML="Please enter Email.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("emailerror").innerHTML="";
		 }
		 
		 if(pass=="")
		 {
			 document.getElementById("passworderror").innerHTML="Please enter password.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("passworderror").innerHTML="";
		 }
		 
		 if(pass.length<8 || pass.length>15)
		 {
			 document.getElementById("passworderror").innerHTML="Password should be between 8 to 15 characters.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("passworderror").innerHTML="";
		 }
		 
		 if(cpass=="")
		 {
			 document.getElementById("cpassworderror").innerHTML="Please re-enter password.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("cpassworderror").innerHTML="";
		 }
		 
		 if(cpass != pass)
		 {
			 document.getElementById("cpassworderror").innerHTML="Confirm password must be same as password.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("cpassworderror").innerHTML="";
		 }
		 
		 return true;
	}

</script>




</head>
<body>
<div>
		<a href="ListServlet.java"><h1>Jw Recipe Web app</h1></a>
	<fieldset>
		
	
			<legend><h1>Sign up</h1></legend>
			
			<%
				String status = (String)request.getAttribute("status");
				if(status != null)
				{
					out.println(status);
				}
			%>
			
			<form onsubmit="return validate()" action="SignUpServlet" method="post">
				<table>
					<tr>
						<td>Email : </td>
						<td><input type="email" id="email" name="email"></td>
						<td><span style="color:red" id="emailerror">*</span></td>
					</tr>
					<tr>
						<td>Password : </td>
						<td><input type="password" id="pass" name="pass"></td>
						<td><span style="color:red" id="passworderror">*</span></td>
					</tr>
					<tr>
						<td>Confirm Password : </td>
						<td><input type="password" id="cpass" name="cpass"></td>
						<td><span style="color:red" id="cpassworderror">*</span></td>
					</tr>	
					
					<tr>
						<td><input type="submit" value="register"></td>
					</tr>
					
					
					
				</table>
			</form>
		
		</fieldset>
</div>

</body>
</html>