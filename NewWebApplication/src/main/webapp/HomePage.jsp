<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
   
    
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Recipe Web App</title>
<link rel="stylesheet" type="text/css" href="home.css">

<style>
table{
	width: 600px;
}

table, th, td{
	border:1px solid #000;
	border-collapse: collapse
}

th,td{
	padding: 10px;
	text-align: center;
}
</style>

</head>
<body>

<div align="right">
	<c:choose>
        <c:when test="${user != null}">
        	<p>Welcome, ${user.email}</p>
			<a href="logout">logout</a>
        </c:when>
        <c:otherwise>
			<a href="Login.jsp" id="login">Login</a>
        </c:otherwise>
    </c:choose>

</div>


<div align="center">
<form name="form1" action="ListServlet" method="post">
        Select a Category:&nbsp;
        <select name="cuisine" onchange="document.form1.submit();">
            <c:forEach items="${listCuisine}" var="cuisine">
                <option value="${cuisine.id}" <c:if test="${cuisine.id eq selectedCuiId}">selected="selected"</c:if>>
                    ${cuisine.cuisine}
                </option>
            </c:forEach>
        </select>
    </form>
    
    <form name="form2" action="NewRecipeServlet" method="get">
        <INPUT TYPE="BUTTON" VALUE="Add New Recipe" ONCLICK="document.form2.submit();">
        <br/><br/>
    </form>
</div>

   
    
    <div align="center">
    	<table>
    		<thead>
    			<tr>
    				<th>Image</th>
    				<th>Name</th>
    				<th>Cuisine</th>
    				<th>Delete</th>
    		</thead> 
    <tbody>
    	<c:forEach items="${listRecipe}" var="recipe">
    		<tr>
    			<td><img src="data:image/jpg;base64,${recipe.img64}" width="120" height="150"/></td>
    			<td><a href="details?id=<c:out value='${recipe.id}'/>">${recipe.name}</a></td>
    			<td>${recipe.cuisine}</td>
    			<td><a href="delete?id=<c:out value='${recipe.id}'/>">Delete</a></td>
    		
    		</tr>
    		</c:forEach>
    </tbody>
    </table>
    
    
    </div>
    
    
    
    
</body>
</html>