<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${recipe.name}</title>
<style>
label {
  width:250px;
  display: block;
}


</style>

</head>
<body>
		<a href="ListServlet.java"><h1>Jw Recipe Web app</h1></a>
		
		<div align="right">
	<c:choose>
        <c:when test="${user != null}">
        	<p>Welcome, ${user.email}</p>
			<a href="logout">logout</a>
        </c:when>
        <c:otherwise>
			<a href="Login.jsp" id="login">Login</a>
        </c:otherwise>
    </c:choose>

</div>
		
	<div align="center">
	
	
	
		<label class="visually-hidden">${recipe.name}</label><br>

		<img src="data:image/jpg;base64,${recipe.img64}" width="300" height="400"/><br>
    	
    	<div>
    	<h3>Upload By:</h3> <label>${recipe.uploader}</label><br>
    	<c:choose>
	        <c:when test="${recipe.editor != null}">
	        	<h3>Last Edit By:</h3> <label>${recipe.editor}</label><br>
	        </c:when>
	        <c:otherwise>
	        
	        </c:otherwise>
   		</c:choose>
    	
    	</div><br>
    	
    	<div>
    	<h3>Ingredients</h3><br>
    	<label>${recipe.ingredient}</label><br>
    	</div>
    	
    	
		<div>
    	<h3>Steps</h3><br>
		<label>${recipe.step}</label><br>
		</div>
	
	<form name="form1" action="UpdateServlet" method="get">
				<INPUT type="hidden" name="id" value="${recipe.id}" id="id" />
				<INPUT TYPE="BUTTON" VALUE="Edit The Recipe" ONCLICK="document.form1.submit();">
	</form><br>
	<form name="form2" action="DetailServlet" method="post">
				<INPUT type="hidden" name="id" value="${recipe.id}" id="id" />
				<INPUT TYPE="BUTTON" VALUE="Delete The Recipe" ONCLICK="document.form2.submit();">
			</form>
	<br>
	</div>
</body>
</html>