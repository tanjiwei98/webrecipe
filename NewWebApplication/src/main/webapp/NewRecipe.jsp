<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add New Recipe</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src='http://js.nicedit.com/nicEdit-latest.js' type="text/javascript"></script>
<script>
bkLib.onDomLoaded(function() { nicEditors.allTextAreas()});
</script>
<script type="text/javascript">
	function validate()
	{
		 var name=document.getElementById("name").value;
		 var image=document.getElementById("fileUpload").value;
		 var ing=document.getElementById("ingredient").value;
		 var step=document.getElementById("step").value;
		 
		 if(name=="")
		 {
			 document.getElementById("nameerror").innerHTML="Please enter name of the dish.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("nameerror").innerHTML="";
		 }
		 
		 if(image=="")
		 {
			 document.getElementById("fileerror").innerHTML="Please upload image.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("fileerror").innerHTML="";
		 }
		 
		 if(ingredient=="")
		 {
			 document.getElementById("ingredienterror").innerHTML="Please enter the ingredient.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("ingredienterror").innerHTML="";
		 }
		 
		 if(step=="")
		 {
			 document.getElementById("steperror").innerHTML="Please enter the step.";
			 return false;
		 }
		 else
		 {
			 document.getElementById("steperror").innerHTML="";
		 }
		 return true;
	}
	
	var MAX_FILE_SIZE = 16177216;
	
	$(document).ready(function() {
	    $('#fileUpload').change(function() {
	        fileSize = this.files[0].size;
	        if (fileSize > MAX_FILE_SIZE) {
	            this.setCustomValidity("File must not exceed 5 MB!");
	            fileUpload.value = "";
	            this.reportValidity();
	        } else {
	            this.setCustomValidity("");
	        }
	    });
	});
	
</script>

</head>
<body>
	<a href="ListServlet.java"><h1>Jw Recipe Web app</h1></a>
<div align="center">
<h3>Add New Recipe</h3>
<form onsubmit="return validate()" enctype="multipart/form-data" action="NewRecipeServlet" method="post">
<label>Select a Category : </label>
        <select name="cuisine">
            <c:forEach items="${listCuisine}" var="cuisine">
                <option value="${cuisine.id}"
                    <c:if test="${cuisine.id eq selectedCatId}">selected="selected"</c:if>
                    >
                    ${cuisine.cuisine}
                </option>
            </c:forEach>
        </select><br>
<label>Name of the dish : </label>
<input type="text" id="name" name="name">
<span style="color:red" id="nameerror">*</span><br>
<label>Image </label>
<input type="file" id="fileUpload" name="image">
<span style="color:red" id="fileerror">*</span><br>
<h3>ingredient : </h3><span style="color:red" id="ingredienterror">*</span><br>
<textarea rows="30" cols="50" name="ingredient" id="ingredient" ></textarea>
<h3>Step : </h3><span style="color:red" id="steperror">*</span><br>
<textarea rows="30" cols="50" name="step" id="step" ></textarea>


<input type="submit" value="Submit">
</form>

</div>

</body>
</html>