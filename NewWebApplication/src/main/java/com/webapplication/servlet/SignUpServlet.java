package com.webapplication.servlet;

import java.io.IOException;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet("/SignUpServlet")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email=request.getParameter("email");

		String pass=request.getParameter("pass");

		String cpass=request.getParameter("cpass");

		PreparedStatement stmt;

		ResultSet rs;

		Connection con;

		RequestDispatcher rd;

		try 

		{

			Class.forName("com.mysql.cj.jdbc.Driver");

			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/db1","root","Pass@word1");

			String sql="insert into user(email,password) values(?,?)";

			stmt=con.prepareStatement(sql);

			stmt.setString(1,email);

			stmt.setString(2,pass);

			int row=stmt.executeUpdate();

			if(row>0)

			{

				request.setAttribute("status","Successfully Signed up... Now you can Login.");

				rd=request.getRequestDispatcher("Login.jsp");

				rd.forward(request, response);

			}		

			else

			{

				request.setAttribute("status","Failed to sign up....! Please Try again.");

				rd=request.getRequestDispatcher("signup.jsp");

				rd.forward(request, response);		

			}

		} 

		catch (Exception e) 

		{

			e.printStackTrace();

		}
	}

}
