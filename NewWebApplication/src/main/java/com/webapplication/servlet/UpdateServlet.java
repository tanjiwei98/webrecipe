package com.webapplication.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webapplication.Class.Recipe;
import com.webapplication.Class.User;
import com.webapplication.dao.RecipeDAO;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		Recipe Recipe = RecipeDAO.selectRecipe(id);
		
		request.setAttribute("recipe", Recipe);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("Update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		String ingre = request.getParameter("ingredient");
		
		String step = request.getParameter("step");
		
		User user = (User) request.getSession().getAttribute("user");
		
		String userEmail = null;
		
		if(user != null) {
			userEmail = user.getEmail();
		}else {
			userEmail = "Guest";
		}	
		
		RecipeDAO.updateRecipe(userEmail,ingre,step,id);
		
		Recipe Recipe = RecipeDAO.selectRecipe(id);
		
		request.setAttribute("recipe", Recipe);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("Detail.jsp");
		
		dispatcher.forward(request, response);
	}

}
