package com.webapplication.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webapplication.Class.Cuisine;
import com.webapplication.Class.Recipe;
import com.webapplication.Class.User;
import com.webapplication.dao.CuisineDAO;
import com.webapplication.dao.RecipeDAO;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getServletPath();

		try {
			switch (action) {
			case "/details":
				goToDetail(request, response);
				break;
			case "/delete":
				deleteRecipe(request, response);
				break;
			case "/logout":
				
				var session = request.getSession();
				
				session.invalidate();
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("ListServlet");
				dispatcher.forward(request, response);
				break;
			default:
				getList(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}

	private void getList(HttpServletRequest request, HttpServletResponse response)throws SQLException, ServletException, IOException {
		List<Cuisine> listCuisine = CuisineDAO.selectAllCuisine();
		request.setAttribute("listCuisine", listCuisine);

		listCuisine.add(0,new Cuisine(null,"All"));
		
		List<Recipe> listRecipe = RecipeDAO.selectAllRecipe();
		request.setAttribute("listRecipe", listRecipe);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("HomePage.jsp");
		dispatcher.forward(request, response);
	}
	private void goToDetail(HttpServletRequest request, HttpServletResponse response)throws SQLException, ServletException, IOException {
		String id = request.getParameter("id");
		
		Recipe Recipe = RecipeDAO.selectRecipe(id);
		
		request.setAttribute("recipe", Recipe);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("Detail.jsp");
		dispatcher.forward(request, response);
	}
	
	private void deleteRecipe(HttpServletRequest request, HttpServletResponse response)throws SQLException, ServletException, IOException {
		String id = request.getParameter("id");
		RecipeDAO.deleteRecipe(id);
		response.sendRedirect("list");
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cuisineId = request.getParameter("cuisine");
		
		request.setAttribute("selectedCuiId", cuisineId);
		
		if(cuisineId == "" || cuisineId == null) 
		{
			List<Cuisine> listCuisine = CuisineDAO.selectAllCuisine();
			request.setAttribute("listCuisine", listCuisine);

			listCuisine.add(0,new Cuisine(null,"All"));
			List<Recipe> listRecipe = RecipeDAO.selectAllRecipe();
			
			request.setAttribute("listRecipe", listRecipe);
			RequestDispatcher dispatcher = request.getRequestDispatcher("HomePage.jsp");
			dispatcher.forward(request, response);
		}
		else 
		{
			List<Cuisine> listCuisine = CuisineDAO.selectAllCuisine();
			request.setAttribute("listCuisine", listCuisine);

			listCuisine.add(0,new Cuisine(null,"All"));
			List<Recipe> listRecipe = RecipeDAO.selectAllRecipe();
			
			listRecipe.removeIf(x -> (!x.getCuisineId().equalsIgnoreCase(cuisineId)));
			
			request.setAttribute("listRecipe", listRecipe);
			RequestDispatcher dispatcher = request.getRequestDispatcher("HomePage.jsp");
			dispatcher.forward(request, response);
		}
		
		
	}

}
