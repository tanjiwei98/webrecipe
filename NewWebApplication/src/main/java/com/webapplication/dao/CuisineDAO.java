package com.webapplication.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import com.webapplication.Class.Cuisine;



public class CuisineDAO {
	private static String jdbcString = "jdbc:mysql://localhost:3306/db1";
	private static String jdbcUsername = "root";
	private static String jdbcPassword = "Pass@word1";
	
	private static final String SELECT_ALL_Cuisine = "select * from category";
	
	public CuisineDAO() {
	}
	
	protected static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcString, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}
	
	public static List<Cuisine> selectAllCuisine() {

		// using try-with-resources to avoid closing resources (boiler plate code)
		List<Cuisine> cuisine = new ArrayList<>();
		// Step 1: Establishing a Connection
		try (Connection connection = getConnection();

				// Step 2:Create a statement using connection object
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_Cuisine);) {
			System.out.println(preparedStatement);
			// Step 3: Execute the query or update query
			ResultSet rs = preparedStatement.executeQuery();

			// Step 4: Process the ResultSet object.
			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("cuisine");
				cuisine.add(new Cuisine(id, name));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return cuisine;
	}

	private static void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}
	
}
