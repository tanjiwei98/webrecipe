package com.webapplication.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.Part;

import com.webapplication.Class.Cuisine;
import com.webapplication.Class.Recipe;

public class RecipeDAO {
	private static String jdbcString = "jdbc:mysql://localhost:3306/db1";
	private static String jdbcUsername = "root";
	private static String jdbcPassword = "Pass@word1";
	
	private static final String SELECT_ALL_Recipe = "select * from recipe left join category ON recipe.cuisineId = category.id;";
	private static final String DELETE_RECIPE = "delete from recipe where id = ?;";
	private static final String SELECT_RECIPE_BYID = "select * from recipe left join category ON recipe.cuisineId = category.id where recipe.id = ?;";
	private static final String INSERT_RECIPE = "insert into recipe (name,image,uploaded,edit,ingredient,step,cuisineId) values (?,?,?,?,?,?,?) ;";
	private static final String UPDATE_RECIPE = "update recipe set edit = ?, ingredient = ?, step = ? where id = ?; ";
	
	
	
	public RecipeDAO() {
	}
	
	protected static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcString, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}
	
	public static Recipe insertRecipe(String name,Part image,String uploaded,String edit,String ingredient,String step,String cuisineId) throws IOException {

		// using try-with-resources to avoid closing resources (boiler plate code)
		Recipe recipe = null;
		// Step 1: Establishing a Connection
		try (Connection connection = getConnection();
				// Step 2:Create a statement using connection object
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_RECIPE);) {
			preparedStatement.setString(1, name);
			InputStream is = image.getInputStream();
			preparedStatement.setBinaryStream(2, is);
			preparedStatement.setString(3, uploaded);
			preparedStatement.setString(4, edit);
			preparedStatement.setString(5, ingredient);
			preparedStatement.setString(6, step);
			preparedStatement.setString(7, cuisineId);
			// Step 3: Execute the query or update query
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			printSQLException(e);
		}
		return recipe;
	}
	
	public static Recipe updateRecipe(String edit,String ingredient,String step,String id) throws IOException {

		// using try-with-resources to avoid closing resources (boiler plate code)
		Recipe recipe = null;
		try (Connection connection = getConnection();
				// Step 2:Create a statement using connection object
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_RECIPE);) {
			preparedStatement.setString(1, edit);
			preparedStatement.setString(2, ingredient);
			preparedStatement.setString(3, step);
			preparedStatement.setString(4, id);
			// Step 3: Execute the query or update query
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			printSQLException(e);
		}
		return recipe;
	}
	
	public static List<Recipe> selectAllRecipe() {

		// using try-with-resources to avoid closing resources (boiler plate code)
		List<Recipe> recipe = new ArrayList<>();
		// Step 1: Establishing a Connection
		try (Connection connection = getConnection();

				// Step 2:Create a statement using connection object
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_Recipe);) {
			System.out.println(preparedStatement);
			// Step 3: Execute the query or update query
			ResultSet rs = preparedStatement.executeQuery();

			// Step 4: Process the ResultSet object.
			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				Blob img = rs.getBlob("image");
				byte[] imgByte = img.getBytes(1, (int) img.length());
				
				String imgString = Base64.getEncoder().encodeToString(imgByte);

				String upload = rs.getString("uploaded");
				String edit = rs.getString("edit");
				String ingre = rs.getString("ingredient");
				String step = rs.getString("step");
				String cuiId = rs.getString("cuisineId");
				String cuisine = rs.getString("cuisine");
				recipe.add(new Recipe(id, name,imgString,upload,edit,ingre,step,cuiId,cuisine));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return recipe;
	}

	public static boolean deleteRecipe(String id) throws SQLException {
		boolean rowDeleted;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_RECIPE);) {
			statement.setString(1, id);
			rowDeleted = statement.executeUpdate() > 0;
		}
		return rowDeleted;
	}
	
	public static Recipe selectRecipe(String id) {

		// using try-with-resources to avoid closing resources (boiler plate code)
		Recipe recipe = null;
		// Step 1: Establishing a Connection
		try (Connection connection = getConnection();

				// Step 2:Create a statement using connection object
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_RECIPE_BYID);) {
			preparedStatement.setString(1, id);
			System.out.println(preparedStatement);
			// Step 3: Execute the query or update query
			ResultSet rs = preparedStatement.executeQuery();

			// Step 4: Process the ResultSet object.
			while (rs.next()) {
				String recipeId = rs.getString("id");
				String name = rs.getString("name");
				Blob img = rs.getBlob("image");
				byte[] imgByte = img.getBytes(1, (int) img.length());
				
				String imgString = Base64.getEncoder().encodeToString(imgByte);
				String upload = rs.getString("uploaded");
				String edit = rs.getString("edit");
				String ingre = rs.getString("ingredient");
				String step = rs.getString("step");
				String cuiId = rs.getString("cuisineId");
				String cuisine = rs.getString("cuisine");
				recipe= new Recipe(recipeId, name,imgString,upload,edit,ingre,step,cuiId,cuisine);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return recipe;
	}
	
	private static void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}
}
