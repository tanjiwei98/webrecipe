package com.webapplication.Class;

public class Cuisine {
	protected String id;
	protected String cuisine;
	
	public Cuisine() {
	}
	
	public Cuisine(String id, String cuisine) {
		super();
		this.id = id;
		this.cuisine = cuisine;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}
	
}
