package com.webapplication.Class;

import java.io.OutputStream;

public class Recipe {
	protected String id;
	protected String name;
	protected String img64;
	protected String uploader;
	protected String editor;
	protected String ingredient;
	protected String step;
	protected String cuisineId;
	protected String cuisine;
	
	public Recipe() {
	}
	
	public Recipe(String id, String name, String img64,String upload,String edit, String ingredient, String step, String cuisineId, String cuisine) {
		super();
		this.id = id;
		this.name = name;
		this.img64 = img64;
		this.uploader = upload;
		this.editor = edit;
		this.ingredient = ingredient;
		this.step = step;
		this.cuisineId = cuisineId;
		this.cuisine = cuisine;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImg64() {
		return img64;
	}
	public void setImage(String img64) {
		this.img64 = img64;
	}
	public String getUploader() {
		return uploader;
	}
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getIngredient() {
		return ingredient;
	}
	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	public String getCuisineId() {
		return cuisineId;
	}
	public void setCuisineId(String cuisineId) {
		this.cuisineId = cuisineId;
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}
	
	
	
	
	
}
